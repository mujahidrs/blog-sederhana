<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Berita;
use App\Models\Kategori;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $beritas = Berita::all();

        return view('home', compact("beritas"));
    }

    public function add()
    {
        $kategoris = Kategori::all();

        return view("add", compact("kategoris"));
    }

    public function edit($id)
    {
        $kategoris = Kategori::all();
        $berita = Berita::find($id);

        return view("edit", compact("kategoris", "berita"));
    }

    public function store(Request $request)
    {
        $request->validate([
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        $imageName = time().'.'.$request->gambar->extension();  
     
        $request->gambar->move(public_path('thumbnail'), $imageName);


       $content = $request->isi_berita;
       $dom = new \DomDocument();
       $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
       $imageFile = $dom->getElementsByTagName('img');
 
       foreach($imageFile as $item => $image){
           $data = $image->getAttribute('src');
           list($type, $data) = explode(';', $data);
           list(, $data)      = explode(',', $data);
           $imgeData = base64_decode($data);
           $image_name= "/upload/" . time().$item.'.png';
           $path = public_path() . $image_name;
           file_put_contents($path, $imgeData);
           
           $image->removeAttribute('src');
           $image->setAttribute('src', $image_name);
        }
 
       $content = $dom->saveHTML();

        Berita::insert([
            "kategori_id" => $request->kategori_id,
            "judul" => $request->judul,
            "isi_berita" => $content,
            "gambar" => $imageName,
            "created_at" => Carbon::now()
        ]);

        return redirect()->route("home");
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        $imageName = time().'.'.$request->gambar->extension();  
     
        $request->gambar->move(public_path('thumbnail'), $imageName);


       $content = $request->isi_berita;
       $dom = new \DomDocument();
       $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
       $imageFile = $dom->getElementsByTagName('img');
 
       foreach($imageFile as $item => $image){
           $data = $image->getAttribute('src');
           list($type, $data) = explode(';', $data);
           list(, $data)      = explode(',', $data);
           $imgeData = base64_decode($data);
           $image_name= "/upload/" . time().$item.'.png';
           $path = public_path() . $image_name;
           file_put_contents($path, $imgeData);
           
           $image->removeAttribute('src');
           $image->setAttribute('src', $image_name);
        }
 
       $content = $dom->saveHTML();

        Berita::find($id)->update([
            "kategori_id" => $request->kategori_id,
            "judul" => $request->judul,
            "isi_berita" => $content,
            "gambar" => $imageName,
            "created_at" => Carbon::now()
        ]);

        return redirect()->route("home");
    }

    public function delete($id)
    {
        Berita::find($id)->delete();

        return redirect()->route("home");
    }
}
