<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Berita;
use App\Models\Komentar;

class MainController extends Controller
{
    public function index()
    {
        $kategoris = Kategori::all();
        $beritas = Berita::orderBy("created_at", "desc")->get();

        foreach($beritas as $berita){
            $berita->kategori = Kategori::find($berita->kategori_id);
        }

        return view('welcome', compact("kategoris", "beritas"));
    }

    public function search(Request $request)
    {
        $query = $request->cari;

        $kategoris = Kategori::all();
        $beritas = Berita::where("judul", "like", "%$query%")->get();

        foreach($beritas as $berita){
            $berita->kategori = Kategori::find($berita->kategori_id);
        }

        return view('welcome', compact("kategoris", "beritas"));
    }

    public function show($id)
    {
        $kategoris = Kategori::all();
        $berita = Berita::find($id);
        // SELECT * FROM berita WHERE id = $id LIMIT 1
        $berita->kategori = Kategori::find($berita->id);

        return view('news', compact("kategoris", "berita"));
    }

    public function showByCategory($kategori)
    {
        $kategoris = Kategori::all();
        $beritas = Berita::where("kategori_id", $kategori)->get();

        foreach($beritas as $berita){
            $berita->kategori = Kategori::find($berita->kategori_id);
        }
        // SELECT * FROM berita WHERE kategori_id = $kategori

        return view('welcome', compact("kategoris", "beritas"));
    }

    public function komentar(Request $request)
    {
        Komentar::insert([
            "user_id" => $request->user_id,
            "berita_id" => $request->berita_id,
            "komentar_id" => $request->komentar_id,
            "komentar" => $request->komentar
        ]);

        return redirect("/news/".$request->berita_id);
    }
}
