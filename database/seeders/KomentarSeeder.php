<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Komentar;

class KomentarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Komentar::insert([
            "user_id" => 2,
            "komentar" => "Wah bermanfaat nih!",
            "berita_id" => 1,
        ]);

        Komentar::insert([
            "user_id" => 3,
            "komentar" => "Setuju banget!",
            "berita_id" => 1,
            "komentar_id" => 1
        ]);
    }
}
