<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            "name" => "Mujahid Robbani Sholahudin",
            "email" => "mujahidrobbanisholahudin@gmail.com",
            "password" => Hash::make("mujahidrs"),
            "role" => "admin"
        ]);

        User::insert([
            "name" => "Udin",
            "email" => "udin@gmail.com",
            "password" => Hash::make("udin"),
            "role" => "user"
        ]);

        User::insert([
            "name" => "Bani",
            "email" => "bani@gmail.com",
            "password" => Hash::make("bani"),
            "role" => "user"
        ]);
    }
}
