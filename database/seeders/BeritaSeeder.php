<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Berita;
use Carbon\Carbon;

class BeritaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Berita::insert([
            "kategori_id" => 1,
            "judul" => "BRI Liga 1: Tak Kenal Libur, Persib Langsung Latihan Usai Tekuk PSS Sleman",
            "isi_berita" => "Liputan6.com, Bandung- Pelatih Persib Bandung, Robert Alberts memilih untuk tidak meliburkan pemainnya dari sesi latihan bersama. Skuad Pangeran Biru tetap menjalani latihan di Lapangan di FINNS Recreations Club pada Sabtu (12/2/2022) sore. \nSebagaimana diketahui, Persib Bandung berhasil menekuk PSS Sleman pada lanjutan BRI Liga 1 2021/2022 pada Jumat (11/2) lalu dengan skor 2-1. Meski baru saja bertanding, Persib tetap tak meliburkan latihan.",
            "created_at" => Carbon::now()
        ]);

        Berita::insert([
            "kategori_id" => 2,
            "judul" => "Hakim Ketua Terpapar Covid-19, Sidang Vonis Azis Syamsuddin Ditunda",
            "isi_berita" => "Liputan6.com, Jakarta - Sidang vonis terdakwa suap mantan penyidik Komisi Pemberantasan Korupsi (KPK), Azis Syamsuddin yang rencananya akan digelar hari ini, Senin (14/2/2022) terpaksa ditunda. Hakim Anggota Fahzal Hendri mengatakan sidang dijadwalkan ulang pada Kamis, 17 Februari mendatang.\n\"Rencana kita hari ini (putusan) tapi ternyata ketua majelisnya pulang ke Makassar di sana terpapar. Jadi sakit, ini baru saya konfirmasi juga Hakim Ad Hoc Pak Jaini Bashir juga sakit sudah dua hari sepertinya terpapar Covid-19,\" kata Fahzal Hendri dalam keterangannya, Senin (14/2/2022).",
            "created_at" => Carbon::now()
        ]);

        Berita::insert([
            "kategori_id" => 3,
            "judul" => "SNMPTN 2022 Dibuka Hari Ini, LTMPT Minta Siswa Segera Finalisasi Registrasi Akun",
            "isi_berita" => "Liputan6.com, Jakarta - Lembaga Tes Masuk Perguruan Tinggi (LTMPT) mulai membuka pendaftaran Seleksi Nasional Masuk Perguruan Tinggi Negeri (SNMPTN) 2022 pada Senin, 14 Februari 2022. Pendaftaran akan dibuka sampai 28 Februari mendatang.\nSebelum bisa mendaftar SNMPTN 2022, siswa yang telah dinyatakan layak harus melakukan registrasi di akun LTMPT. Sampai Minggu, 13 Februari 2022, tercatat sudah ada 1.364.194 siswa baru yang terdaftar di akun LTMPT.",
            "created_at" => Carbon::now()
        ]);

        Berita::insert([
            "kategori_id" => 3,
            "judul" => "BRI Liga 1: Tak Kenal Libur, Persib Langsung Latihan Usai Tekuk PSS Sleman",
            "isi_berita" => "Liputan6.com, Bandung- Pelatih Persib Bandung, Robert Alberts memilih untuk tidak meliburkan pemainnya dari sesi latihan bersama. Skuad Pangeran Biru tetap menjalani latihan di Lapangan di FINNS Recreations Club pada Sabtu (12/2/2022) sore. \nSebagaimana diketahui, Persib Bandung berhasil menekuk PSS Sleman pada lanjutan BRI Liga 1 2021/2022 pada Jumat (11/2) lalu dengan skor 2-1. Meski baru saja bertanding, Persib tetap tak meliburkan latihan.",
            "created_at" => Carbon::now()
        ]);

        Berita::insert([
            "kategori_id" => 1,
            "judul" => "Hakim Ketua Terpapar Covid-19, Sidang Vonis Azis Syamsuddin Ditunda",
            "isi_berita" => "Liputan6.com, Jakarta - Sidang vonis terdakwa suap mantan penyidik Komisi Pemberantasan Korupsi (KPK), Azis Syamsuddin yang rencananya akan digelar hari ini, Senin (14/2/2022) terpaksa ditunda. Hakim Anggota Fahzal Hendri mengatakan sidang dijadwalkan ulang pada Kamis, 17 Februari mendatang.\n\"Rencana kita hari ini (putusan) tapi ternyata ketua majelisnya pulang ke Makassar di sana terpapar. Jadi sakit, ini baru saya konfirmasi juga Hakim Ad Hoc Pak Jaini Bashir juga sakit sudah dua hari sepertinya terpapar Covid-19,\" kata Fahzal Hendri dalam keterangannya, Senin (14/2/2022).",
            "created_at" => Carbon::now()
        ]);

        Berita::insert([
            "kategori_id" => 2,
            "judul" => "SNMPTN 2022 Dibuka Hari Ini, LTMPT Minta Siswa Segera Finalisasi Registrasi Akun",
            "isi_berita" => "Liputan6.com, Jakarta - Lembaga Tes Masuk Perguruan Tinggi (LTMPT) mulai membuka pendaftaran Seleksi Nasional Masuk Perguruan Tinggi Negeri (SNMPTN) 2022 pada Senin, 14 Februari 2022. Pendaftaran akan dibuka sampai 28 Februari mendatang.\nSebelum bisa mendaftar SNMPTN 2022, siswa yang telah dinyatakan layak harus melakukan registrasi di akun LTMPT. Sampai Minggu, 13 Februari 2022, tercatat sudah ada 1.364.194 siswa baru yang terdaftar di akun LTMPT.",
            "created_at" => Carbon::now()
        ]);
    }
}
