@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Tambah Berita') }}
                </div>

                <div class="card-body">
                  <form method="POST" action="/store" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                      <label for="exampleFormControlInput1" class="form-label">Judul Berita</label>
                      <input name="judul" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan judul berita">
                    </div>
                    <div class="mb-3">
                      <label for="exampleFormControlInput1" class="form-label">Kategori Berita</label>
                      <select name="kategori_id" class="form-control">
                        <option value="">Pilih Opsi</option>
                        @foreach ($kategoris as $kategori)
                          <option value="{{ $kategori->id }}">{{ $kategori->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="mb-3">
                      <label for="exampleFormControlInput1" class="form-label">Thumbnail</label>
                      <input name="gambar" type="file" class="form-control" id="exampleFormControlInput1" placeholder="Upload gambar thumbnail">
                    </div>
                    <div class="mb-3">
                      <label class="form-label">Isi Berita</label>
                      <textarea name="isi_berita" id="summernote"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Kirim</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
