<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portal Berita</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-danger">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Portal Berita</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/">Beranda</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Kategori
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @foreach ($kategoris as $kategori)
                        <li><a class="dropdown-item" href="#">{{ $kategori->nama }}</a></li>
                    @endforeach
                </ul>
              </li>
            </ul>
            <form class="d-flex">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-warning" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>

      <div class="container my-3">
          <h1>{{ $berita->judul }}</h1>
          <p>{{ $berita->created_at }}</p>
          <img src="{{ asset("/thumbnail/".$berita->gambar) }}" height="200"/>
          <p>{!! $berita->isi_berita !!}</p>

          <form method="POST" action="/komentar">
            @csrf
          
              <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Komentar</label>
                <input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
                <input type="hidden" value="{{ $berita->id }}" name="berita_id">
                <input type="hidden" value="" name="komentar_id">
                <textarea class="form-control" name="komentar"></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Komentar</button>
          </form>

          @foreach ($berita->comments as $komen)
            @if($komen->komentar_id === null)
            <div class="card">
              <div class="card-header">
                {{ $komen->user->name }}
              </div>
              <div class="card-body">
                <p>{{ $komen->komentar }}</p>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#reply-{{ $komen->id }}">
                  Reply
                </button>

                <!-- Modal -->
                <div class="modal fade" id="reply-{{ $komen->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Balas Komentar</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <form method="POST" action="/komentar">
                        @csrf
                      <div class="modal-body">
                          <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Komentar</label>
                            <input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
                            <input type="hidden" value="{{ $berita->id }}" name="berita_id">
                            <input type="hidden" value="{{ $komen->id }}" name="komentar_id">
                            <textarea class="form-control" name="komentar"></textarea>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                  @foreach ($komen->comments as $reply)
                  <div class="card ms-3">
                    <div class="card-header">
                      {{ $reply->user->name }}
                    </div>
                    <div class="card-body">
                      <p>{{ $reply->komentar }}</p>
                    </div>
                  </div>
                @endforeach
            @endif
          @endforeach
      </div>
</body>
</html>