<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portal Berita</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-danger">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Portal Berita</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/">Beranda</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Kategori
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @foreach ($kategoris as $kategori)
                        <li><a class="dropdown-item" href="{{ "/" . $kategori->id }}">{{ $kategori->nama }}</a></li>
                    @endforeach
                </ul>
              </li>
            </ul>
            <form class="d-flex" method="POST" action="/">
              @csrf
              <input name="cari" class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-warning" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>

      <div class="container">
          <div class="row">
          @foreach ($beritas as $berita)
            <div class="col col-md-6 col-sm-12 col-xs-12 mb-3 mt-2">
                <div class="card">
                    <div class="card-header">
                        {{ $berita->kategori->nama }}
                    </div>
                    <img src="{{ asset("/thumbnail/".$berita->gambar) }}" class="card-img-top" alt="..." height="200">
                    <div class="card-body">
                    <h5 class="card-title">{{ $berita->judul }}</h5>
                    <p class="card-text">{{ $berita->created_at }}</p>
                    <a href="/news/{{ $berita->id }}" class="btn btn-primary">Read More</a>
                    </div>
                </div>
            </div>
          @endforeach
          </div>
      </div>
</body>
</html>