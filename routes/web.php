<?php

use Illuminate\Support\Facades\Route;
use App\Models\Kategori;
use App\Models\Berita;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\MainController::class, 'index']);
Route::post('/', [App\Http\Controllers\MainController::class, 'search']);
Route::get('/news/{id}', [App\Http\Controllers\MainController::class, 'show']);
Route::get('/kategori/{kategori}', [App\Http\Controllers\MainController::class, 'showByCategory']);
Route::post('/komentar', [App\Http\Controllers\MainController::class, 'komentar']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/add', [App\Http\Controllers\HomeController::class, 'add'])->name('add');
Route::get('/edit/{id}', [App\Http\Controllers\HomeController::class, 'edit'])->name('edit');
Route::post('/store', [App\Http\Controllers\HomeController::class, 'store'])->name('store');
Route::post('/update/{id}', [App\Http\Controllers\HomeController::class, 'update'])->name('update');
Route::get('/delete/{id}', [App\Http\Controllers\HomeController::class, 'delete'])->name('delete');
